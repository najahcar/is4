<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 2</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 2</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* Hacer un script PHP que imprime la siguiente información:
        • Versión de PHP utilizada.
        • El id de la versión de PHP.
        • El valor máximo soportado para enteros para esa versión.
        • Tamaño máximo del nombre de un archivo.
        • Versión del Sistema Operativo.
        • Símbolo correcto de 'Fin De Línea' para la plataforma en uso.
        • El include path por defecto.
        Observación: Ver las constantes predefinidas del núcleo de PHP */

        echo "Version de PHP ID: ".PHP_RELEASE_VERSION."<br />";
  			echo "ID de la version de PHP: ".PHP_VERSION_ID."<br />";
  			echo "Valor maximo de enteros soportado: ".PHP_INT_MAX."<br />";
  			echo "Tamaño máximo del nombre de un archivo: ".PHP_MAXPATHLEN."<br />";
  			echo "Versión del Sistema Operativo: ".PHP_OS."<br />";
  			echo "Símbolo correcto de 'Fin De Línea' para la plataforma en uso: ".PHP_EOL."<br />";
  			echo "El include path por defecto: ".DEFAULT_INCLUDE_PATH."<br />";
    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
