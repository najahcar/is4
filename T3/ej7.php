<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 7</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 7</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* Hacer un script en PHP que determine si una cadena dada es un palíndromo
    Un palíndromo (del griego palin dromein, volver a ir hacia atrás) es una palabra, número o frase
    que se lee igual hacia adelante que hacia atrás. Si se trata de un número, se llama capicúa.
    Habitualmente, las frases palindrómicas se resienten en su significado cuanto más largas son
    Ejemplos: radar, Neuquén, anilina, ananá, Malayalam
    Obs: El alumno deberá crear sus propias funciones para realizar este ejercicio.*/
function is_palindromo($p){
  $minusculas = explode(" ", strtolower($p)); //Convertimos la cadena a minusculas
  $nuevo="";
  foreach($minusculas as $m)
  {
    trim($m); //Eliminamos los espacios en blanco
    $nuevo .= $m;
  }
  $palabra_inv = strrev($nuevo);
  if ( $nuevo==$palabra_inv)
  {
    echo 'La palabra '.$p.' es un palíndromo';
  }
  else
  {
    echo 'La palabra '.$p.' no es un palíndromo';
  }
}
$FORM=<<<EOD
    <form method="post">
        Palabra: <input type="text" name="palabra"><br /><br />
        <input type="submit" value="enviar">
    </form>
EOD;
    echo $FORM;
    if (empty($_POST['palabra']) ){
      echo "Ingresar Palabra";
    } else {
      $palabra=$_POST['palabra'];
      is_palindromo($palabra);
    }
    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
