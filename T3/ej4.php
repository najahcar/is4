<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 4</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 4</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* Hacer un script PHP que haga lo siguiente:
    • Declarar 3 variable con valores enteros aleatorios (se debe generar aleatoriamente estos
    valores entre 50 y 900) – Se debe usar la función mt_srand y mt_rand.
    • Ordenar de mayor a menor los valores de estas variables.
    • Imprimir en pantalla la secuencia de números ordenadas, los números deben estar
    separados por espacios en blanco.
    • El número mayor debe estar en color verde y el número más pequeño debe estar en color
    rojo.  */
    mt_srand( time());
    $a=mt_rand(50,900);
    $b=mt_rand(50,900);
    $c=mt_rand(50,900);
    if ($a>$b and $a>$c and $b>$c)
    {
      echo '<span style="color:green;text-align:center;">'.$a.'</span> '.$b.' <span style="color:red;text-align:center;">'.$C.'</span>';
    }
    if ($a>$b and $a>$c and $c>$b)
    {
      echo '<span style="color:green;text-align:center;">'.$a.'</span> '.$c.' <span style="color:red;text-align:center;">'.$b.'</span>';
    }
    if ($b>$a and $b>$c and $a>$c)
    {
      echo '<span style="color:green;text-align:center;">'.$b.'</span> '.$a.' <span style="color:red;text-align:center;">'.$c.'</span>';
    }
    if ($b>$a and $b>$c and $c>$a)
    {
      echo '<span style="color:green;text-align:center;">'.$b.'</span> '.$c.' <span style="color:red;text-align:center;">'.$a.'</span>';
    }
    if ($c>$a and $c>$b and $a>$b)
    {
      echo '<span style="color:green;text-align:center;">'.$c.'</span> '.$a.' <span style="color:red;text-align:center;">'.$b.'</span>';
    }
    if ($c>$a and $c>$b and $b>$a)
    {
      echo '<span style="color:green;text-align:center;">'.$c.'</span> '.$b.' <span style="color:red;text-align:center;">'.$a.'</span>';
    }

    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
