<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 9</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 9</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
  <?php
    /* Realizar un script en PHP que declare un vector de 100 elementos con valores aleatorios enteros
    entre 1 y 100 y sume todos los valores. El script debe imprimir un mensaje con la sumatoria de los
    elementos
    Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. */
function crear_vector(){
    mt_srand( time());
    for ($i=1; $i<=100; $i++)
    {
      $vector [$i] = mt_rand(1,100);
    }
    $resultado=0;
    for ($i=1; $i<=100; $i++)
    {
      echo $vector [$i]."  ";
      $resultado += $vector [$i];
      }
    echo '<br />Resultado de la suma: '.$resultado;
}
crear_vector();
  ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
