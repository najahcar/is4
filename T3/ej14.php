<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 14</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 14</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
  <?php
    /* Crear un array asociativo en PHP que cumpla los siguientes requisitos:
    • Los índices/claves deben ser strings (estas cadenas deben ser generadas aleatoriamente
    por el script y deben tener una longitud de 5 a 10 caracteres).
    • Los valores deben ser números enteros del 1 a 1000.
    • Se debe imprimir el vector completo indicando índice y valor.
    • Se debe recorrer dicho array e imprimir sólo las claves del array que empiecen con la letra
    a, d, m y z (función imprimir) (en el caso se no existir ningún índice que comience con esas
    letras se debe imprimir un mensaje).
    Observación: Se debe crear un archivo por función y el archivo principal donde se llaman a las
    funciones y se realizan los procesamientos. */
    include 'FUNCIONES/generar_array.php';
    include 'FUNCIONES/imprimir_vector.php';
    include 'FUNCIONES/imprimir_buscados.php';

    $vector=generar_array();
    imprimir_vector($vector);
    imprimir_buscados($vector);

  ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
