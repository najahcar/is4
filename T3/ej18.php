<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 18</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 18</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
<?php
    /* Realizar un contador de visitas en PHP utilizando un archivo para mantener la cantidad de visitas a
    la página.
    Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. */

require 'FUNCIONES/abrir_archivo.php';

function imprimir_visitas()
{
	$dir      = "ARCHIVOS/contador.txt";
	$gestor = abrir_archivo($dir);

	if( $gestor == -1 )
	{
		echo "No se puede abrir el archivo.";
	}
  else {
    $cant_visitas = (integer)fgets($gestor);
  	rewind($gestor);
  	$cant_visitas += 1;
  	fwrite($gestor, $cant_visitas);
  	fclose($gestor);
  	echo "Total visitas: $cant_visitas.";
  }
}

imprimir_visitas();

?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
