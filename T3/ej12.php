<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 12</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 12</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
  <?php
    /* Hacer un script PHP que haga lo siguiente:
    • Crear un array indexado con 10 cadenas.
    • Crear una variable que tenga un string cualquiera como string.
    El script debe:
    • Buscar si la cadena declarada está en el vector declarado.
    • Si esta, se imprime “Ya existe” la cadena.
    • Si no está, se imprime “Es Nuevo” y se agrega al Final del array.
    Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. */

$v1 = array("abc","bcd","cde","def","efg","fgh","ghi","hij","ijk","jkl");
$var1 = "klm";
print_r($v1);
function existe($var,$v){
  $a=false;
  while ($a===false) {
    foreach ($v as $key => $value) {
      if ($var === $value) {
        $a=true;
        echo "<br />Ya existe.<br />";
        break;
      } else {
        $a=true;
        echo "<br />Es nuevo.<br />";
        $v[]=$var;
        break;
      }
    }
  }
  print_r($v);
}
existe($var1,$v1);

  ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
