<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 20</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 20</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <!--  Implementar un script PHP que implemente una funcionalidad básica de login
• El formulario debe tener dos campos: usuario, contraseña.
• El formulario deberá tener un botón de login.
• La verificación de usuario y contraseña se realizará a través de un archivo de datos (ya sea
.txt, .dat).
• Si el usuario introducido existe en el archivo, se debe re-enviar a una página de
bienvenida.
• Si el usuario introducido no existe en el archivo, se debe imprimir un mensaje de error de
acceso. -->

    <form role="form" method="POST" action="FUNCIONES/login.php">
      <input type = "text" name = "email" placeholder = "email" required autofocus>
       <br><br>
   <input type = "password" name = "password" placeholder = "contraseña" required>
      <br><br>
      <button type = "submit" name = "login">Login</button>
    </form>

  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
