<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 10</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 10</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
  <?php
    /* Hacer un script PHP que declare un vector de 50 elementos con valores aleatorios enteros entre 1
    y 1000. El script debe determinar cuál es el elemento con mayor valor en el vector, se debe
    imprimir un mensaje con el valor y el índice en donde se encuentra.
    Mensaje de ejemplo: El elemento con índice 8 posee el mayor valor que 789.
    Obs: El alumno deberá crear sus propias funciones para realizar este ejercicio. */
function crear_vector(){
  for ($j=1; $j<=50; $j++)
    {
      $vector [$j] = rand(1,1000);
    }
    return $vector;
}
function buscar_mayor($v){
  $valor_mayor= $v [1];
  for ($j=1; $j<=50; $j++)
    {
      echo $v [$j]."	";
      if ($v [$j] > $valor_mayor)
      {
        $valor_mayor=$v [$j];
        $posicion = $j;
      }
    }
  echo '<br />El numero mayor es '.$valor_mayor.' que se encuentra en la posicion '.$posicion;
}
$array1=crear_vector();
buscar_mayor($array1);


  ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
