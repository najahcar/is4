<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 8</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 8</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* Hacer un script en PHP que genere una matriz de dimensión n*m con números aleatorios. Las
    dimensiones n (filas) y m (columnas) son variables que debe definir el alumno. La matriz generada
    se debe imprimir en pantalla de manera tabular.
    Obsevación: El alumno deberá crear sus propias funciones para realizar este ejercicio. */

    function generar_matriz($m,$n){
      for ($i=1; $i<=$m; $i++)
      {
        for ($j=1; $j<=$n; $j++)
        {
          $valor [$i] [$j] = "[$i $j]";
        }
      }
      for ($i=1; $i<=$m; $i++)
      {
        for ($j=1; $j<=$n; $j++)
        {
          echo $valor [$i] [$j];
          echo "	";
        }
        echo '<br>';
      }
    }
$form=<<<EOD
      <form method="post">
        <p>
          Valor 1: <input type="text" name="m">
        </p>
        <p>
          Valor 2: <input type="text" name="n">
        </p>
        <input type="submit" value="enviar">
      </form>
        <br>
EOD;
  echo $form;
    if (empty($_POST['m']) && empty($_POST['n'])) {
      echo "Inserte valores.";
    } else {
      $m=htmlspecialchars($_POST['m']);
      $n=htmlspecialchars($_POST['n']);
      generar_matriz($m,$n);
    }


    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
