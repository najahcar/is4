<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 20</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 20</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <!--  Leer un archivo agenda.txt (creado por el alumno), este archivo contendrá el nombre y el apellido
    de personas.
    Se debe crear una función PHP que busque un nombre y un apellido en dicho archivo e imprima un
    mensaje si se encontró o no se encontró el nombre y apellido en el archivo. Nombre y apellido son
    variables que se debe introducir el usuario por formulario.
    Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->
    <form role="form" method="POST" action="FUNCIONES/agenda.php">
      <label for="nombre">Nombre:</label>
      <input type="text" name="nombre">
      <label for="apellido">Apellido:</label>
      <input type="text" name="apellido">
      <button type="submit">Buscar</button>
    </form>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
