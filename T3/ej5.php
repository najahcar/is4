<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 5</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 5</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* Hacer un script PHP que genere un formulario HTML que contenga los siguientes campos: nombre,
      apellido, edad y el botón de submit.
      • Se deben usar las cadenas HEREDOC. */
$FOO=<<<EOD
    <form action="ejemplo.php" method="get">
      <p>Nombre: <input type="text" name="nombre" size="40"></p>
      <p>Apellido: <input type="text" name="apellido" size="40"></p>
      <p>Edad: <input type="number" name="edad" min="1" max="70"></p>
      <p><input type="submit" value="Enviar"></p>
    </form>
EOD;
echo $FOO;

    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
