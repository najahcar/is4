<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 21</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 21</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <!--  Hacer un formulario HTML que posee los siguientes tipos de elementos (el alumno debe crear un
          formulario que tenga sentido):
          • Un campo de Texto usuario
          • Un campo Password
          • Un select Simple nacionalidad
          • Un select múltiple
          • Un Checkbox
          • Un Radio Button sexo
          • Un Text Area
          Implementar un script PHP que capture los datos del anterior formulario e imprima en pantalla lo
          que introdujo el usuario de manera legible. -->

    <form role="form" method="POST" action="FUNCIONES/capture_datos.php">
      <p><label for="usuario">Usuario:</label><input type="text" name="usuario"></p>
      <p><label for="password">Password:</label><input type="password" name="password"></p>
      <p><label for="nacionalidad">Nacionalidad:</label>
        <select name="nacionalidad">
          <option value="paraguaya">Paraguay</option>
          <option value="argentina">Argentina</option>
          <option value="brasil">Brasil</option>
          <option value="chile">Chile</option>
      </select> </p>
      <br>
      <p>Seleccione sus niveles educativos:</p>
      <input type="checkbox" name="primaria" value="primaria">
      <label for="Primaria">Primaria</label><br>
      <input type="checkbox" name="secundaria" value="secundaria">
      <label for="Secundaria">Secundaria</label><br>
      <input type="checkbox" name="terciaria" value="terciaria">
      <label for="terciaria">Terciaria</label><br>
      <input type="checkbox" name="universitaria" value="universitaria">
      <label for="universitaria">Universitaria</label><br>
      <input type="checkbox" name="postgrado" value="postgrado">
      <label for="postgrado">Postgrado</label><br>
      <p>Seleccione su genero:</p>
      <input type="radio" name="genero" value="masculino">
      <label for="masculino">Masculino</label><br>
      <input type="radio" name="genero" value="femenino">
      <label for="femenino">Femenino</label><br>
      <p>Ingrese una observación si lo desea</p>
      <label for="observacion"></label>
      <textarea name="observacion" rows="4" cols="50"></textarea>
      <br><br>
      <button type = "submit" name = "guardar-datos">Guardar</button>
    </form>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
