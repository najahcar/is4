<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 3</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 3</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* • Generar un script PHP que cree una tabla HTML con los números pares que existen entre 1 y N.
        • El número N estará definido por una constante PHP. El valor de la constante N debe ser un
        número definido por el alumno.
        El script PHP debe estar embebido en una página HTML. */

        define('N', '9');
  			echo 'Cantidad de N ingresado: '.N.'<br />';
  			echo '<table><tr><th>Numero par</th></tr>';
  			for($i=1; $i<=N; $i++)
  			{
  				if ($i%2==0)
  				{
  					echo  '<tr><td>'.$i.'</td></tr>';
  				}
  			}
  			echo '</table>';
    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
