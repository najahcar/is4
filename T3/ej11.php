<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 11</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 11</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
  <?php
    /* Realizar un script PHP que haga lo siguiente:
    Declare un vector indexado cuyos valores sean urls (esto simula un log de acceso de un usuario).
    El script debe contar e imprimir cuantas veces accedió el usuario al mismo url (no se deben repetir
    los url).
    Obs: El alumno deberá crear sus propias funciones para realizar este ejercicio. */

$urls = array("https://www.gitlab.com","https://www.mail.google.com.py","https://www.abc.com.py","https://www.mail.google.com.py","https://www.abc.com.py","https://www.mail.google.com.py","https://www.mail.google.com.py","https://facebook.com","https://youtube.com","https://facebook.com","https://youtube.com");
function verificar_log($v){
  $v1=array_count_values($v);
  $i=0;
  foreach ($v1 as $key => $value) {
    $i++;
    echo $i." - ".$key." accesos: ".$value."<br />";
  }
}
verificar_log($urls);

  ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
