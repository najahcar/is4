<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 6</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 6</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* Hacer un script en PHP que simule una calculadora con las operaciones básicas: suma, resta,
    multiplicación y división.
    • El usuario debe introducir el operando 1.
    • El usuario debe introducir el operando 2.
    • El usuario debe seleccionar la operación que quiere realizar (a través de un select).
    • El usuario debe presionar el botón calcular.
    • El script debe calcular el resultado de la operación e imprimirlo en pantalla. El script debe
    realizar los controles respectivos (para casos donde las operaciones no son válidas). */
$BOO=<<<EOD
    <br />
    <form method="post">
        Operando 1: <input type="text" name="operando1"><br /><br />
        Operador: <select name="operador">
          <option value="+">+
          </option>
          <option value="-">-
          </option>
          <option value="*">*
          </option>
          <option value="/">/
          </option>
        </select><br /><br />
        Operando 2: <input type="text" name="operando2"><br /><br />
        <input type="submit" value="enviar">
    </form>
EOD;
echo $BOO;
    echo '<br>';
    if(!empty($_POST ["operando1"]) and !empty($_POST ["operando2"])){
      if ($_POST["operador"] == "+") {
        echo ($resultado = $_POST ["operando1"] + $_POST ["operando2"]);
      }
      elseif ($_POST["operador"] == "-") {
        echo ($resultado = $_POST ["operando1"] - $_POST ["operando2"]);
      }
      elseif ($_POST["operador"] == "*") {
        echo ($resultado = $_POST ["operando1"] * $_POST ["operando2"]);
      }
      elseif ($_POST["operador"] == "/") {
        echo ($resultado = $_POST ["operando1"] / $_POST ["operando2"]);
      }
    }
    else{
      echo "Ingrese datos.";
    }
    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
