<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 13</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 13</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
  <?php
    /* Crear un array asociativo de 10 elementos. Los valores de los 10 elemento del array deben ser
    strings.
    Crear un array asociativo de un elemento. El valor del array debe ser un string.
    El script PHP debe hacer lo siguiente:
    • Imprimir en pantalla si existe la clave del array de un elemento en el array de 10
    elementos (si no existe, se imprime un mensaje).
    • Imprimir en pantalla si existe el valor del vector de un elemento en el vector de 10
    elementos (si no existe, se imprime un mensaje).
    • Si no existe ni la clave ni el valor (del vector de un elemento) en el vector mayor se inserta
    como un elemento más del vector. */

$capitales = array("paraguay"=>"asuncion","brasil"=>"brasilia","argentina"=>"buenos aires","ecuador"=>"quito","uruguay"=>"montevideo","mexico"=>"mexico","bolivia"=>"sucre","canada"=>"otawa","chile"=>"santiago","colombia"=>"bogota");
$cap1 = array("estados unidos"=>"washington");
print_r($capitales);
function existe($capitales,$capi1){
  $x = 0;
  $y = 0;
  $v=each($capi1);
  foreach ($capitales as $key => $value) {
    if ($v['key']==$key)
    {
      $x=1;
    }
    if ($v['value']==$value)
    {
      $y=1;
    }
  }
  if ($x == 1)
  {
    echo 'Existe la clave en el array<br />';
  }
  else
  {
    echo 'No existe la clave en el array<br />';
  }
  if ($y == 1)
  {
    echo 'Existe el valor en el vector<br />';
  }
  else
  {
    echo 'No existe el valor en el vector<br />';
  }
  if ($x == 0 && $y == 0)
  {
    echo 'Se agrega el nuevo string en el array'.'<br />';
    $capitales[$v['key']]=$v['value'];
  }
  print_r($capitales);
}
existe($capitales,$cap1);

  ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
