<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 19</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 19</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
<?php
    /* Dado un archivo con la siguiente información: una lista de matrículas, nombre, apellido y 3 notas
    parciales para un grupo alumnos (el archivo puede tener un número variable de líneas de texto).
    Procesar estos datos y construir un nuevo archivo que contenga solamente la matrícula y la
    sumatoria de notas de los alumnos. */

require 'FUNCIONES/abrir_archivo.php';
require 'FUNCIONES/imprimir_archivo.php';

function cargar_notas($dir_lista,$dir_notas)
{
	$gestor_lista = abrir_archivo($dir_lista);
	$gestor_notas = abrir_archivo($dir_notas);

	if( ($gestor_lista == -1) or ($gestor_notas == -1) )
		die('No se puede abrir archivo.');

	$notas = file($dir_lista);
	foreach ( $notas as $value )
	{
		list($matricula,$nombre,$apellido,$puntaje1,$puntaje2,$puntaje3) = explode(" ", $value);
		$acumulado = (int)$puntaje1 + (int)$puntaje2 + (int)$puntaje3;
		$cadena = $matricula ." ". $acumulado."\n";
		fwrite($gestor_notas, $cadena, strlen($cadena));
	}

	fclose($gestor_notas);
	fclose($gestor_lista);

}

$dir_lista   = "ARCHIVOS/lista_puntajes.txt";
$dir_notas = "ARCHIVOS/notas.txt";
cargar_notas($dir_lista,$dir_notas);
imprimir_archivo($dir_lista);
imprimir_archivo($dir_notas);

?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
