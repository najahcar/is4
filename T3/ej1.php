<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 1</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 1</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* Hacer un script PHP que realice el siguiente cálculo
      x = ((A * ¶) + B) / (C*D) - Se debe calcular e imprimir el valor de x
      Donde:
      • A es la raiz cuadrada de 2
      • ¶ es el número PI
      • B es es la raíz cúbica de 3
      • C es la constante de Euler
      • D es la constante e
      Observación: Utilizar las constantes matemáticas definidas den la extensión math de PHP */

      function raiz_cubica($numero){  //funcion para calcular la raiz cubica de un numero
				return pow($numero, 1/3);     //retorna número elevado a la potencia 0.333333333
			}
			$A=M_SQRT2;                     //constante raiz cuadrada de 2
      $¶=M_PI;                        //constante de PI
			$B=raiz_cubica(3);              //calcula la raiz cubica de 3
			$C=M_EULER;                    //constante de EULER
			$D=M_E;                        //constante de e
			$x =(($A * $¶)+$B)/($C*$D);    //variable con resultado de ejercicio
			echo "Resultado= ".$x;        //imprimir resultado
    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
