<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 15</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 15</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
  <?php
    /*  Declarar un vector de 20 elementos con valores aleatorios de 1 a 10.
    Crear una función recursiva en PHP que recorra el array de atrás para adelante. Se deberá
    imprimir desde el último elemento del array hasta el primero
    Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. */
    include 'FUNCIONES/imprimir_vector.php';

    function recorrer($v,$nro){

      if ($nro>-1) {
        echo " Valor: ".$v[$nro]."<br />";
        recorrer($v,--$nro);
      }
    }
    mt_srand( time());
    for ($i=0; $i <20 ; $i++) {
      $aux= mt_rand( 1,10);
      $vector[]=$aux;
    }
    imprimir_vector($vector);
    recorrer($vector,count($vector)-1);

  ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
