<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 4 - Ejercicio 5</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 5</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* • Crear una BD en postgreSQL llamada ejercicio4 (usar el pgAdmin)
• Crear una tabla con tres campos:
o id: campo de tipo serial y que sea PK
o nombre: un campo que sea una cadena de 40 caracteres
o Descripción: un campo que sea un varchar de 250 caracteres
• Hacer un script PHP que inserte 1000 registros en la tabla (los valores deben ser aleatorios
y deben ser generados automáticamente con el script).
• Hacer un script PHP que visualice esos 1000 registros insertados previamente. La
visualización se deberá hacer de manera tabular */

function generate_string($input, $long) {
    $input_long = strlen($input);
    $random_string = '';
    for($i = 0; $i < $long; $i++) {
        $random_character = $input[mt_rand(0, $input_long - 1)];
        $random_string .= $random_character;
    }
    return $random_string;
}
$caracteres_permitidos = 'abcdefghijklmnopqrstuvwxyz';
mt_srand( time());

//conexion a la BD
$conn_string = "host=localhost port=5432 dbname=ejercicio4 user=postgres password=postgres";
$conexion = pg_connect($conn_string);
//verificar que no hubo error de conexion
if (!$conexion) {
  echo "Ocurrio un error de conexion";
  exit;
}
$a=1;
while ($a <= 1000) {
  $longitud_nombre= mt_rand( 1,40);
  $longitud_descripcion= mt_rand( 1,250);
  $nombre=generate_string($caracteres_permitidos,$longitud_nombre);
  $descripcion=generate_string($caracteres_permitidos, $longitud_descripcion);
  $sql= "insert into lista (id, nombre, descripcion) values ('$a', '$nombre', '$descripcion')";
  $datos = pg_query($conexion, $sql);
  if (!$datos) {
    echo "Ocurrio un error en la insercion";
    exit;
  }
  $a++;

}
$query = "select * from Lista order by 1";
$datos = pg_query($conexion, $query);
//verificar que no hubo error en la consulta
if (!$datos) {
  echo "Ocurrio un error en la consulta";
  exit;
}
// imprimimos resultado en tabla
echo "<table><tr><th>ID</th><th>NOMBRE</th><th>DESCRIPCION</th></tr>";
while($row = pg_fetch_array($datos)){
  echo "<tr>";
  echo "<td>$row[0]</td>";
  echo "<td>$row[1]</td>";
  echo "<td>$row[2]</td>";
  echo "</tr>";
}
echo "</table>";
pg_close( $conexion);
    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
