<?php
/* Este archivo se encarga de conectar a la base de datos y traer un objeto PDO
================================
    Hacer un script en PHP que se conecte a la BD, anteriormente creada en el “ejercicio1” y
    haga lo siguiente:
    o Realizar una consulta a dicha BD para que traiga los siguientes datos (respetar
    orden):
    ▪ nombre_producto,precio_producto,nombre_marca,
    nombre_empresa,nombre_categoría

    o Presentar los datos de manera tabular (usando tablas HTML generadas a partir de
    PHP)

    Aclaración: Usar la extensión de pgsql */
$contrasena = "postgres";
$usuario = "postgres";
$nombreBaseDeDatos = "ejercicio1";
$rutaServidor = "127.0.0.1";
$puerto = "5432";

try {
  $bd = new PDO("pgsql:host=$rutaServidor;port=$puerto;dbname=$nombreBaseDeDatos", $usuario, $contrasena);
  $bd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  echo "Conexion exitosa";
} catch (Exception $e) {
  echo "Ocurrió un error con la conexion a la base de datos: " . $e->getMessage();
}
?>
