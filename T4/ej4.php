<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 4 - Ejercicio 4</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 4</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* Realizar el ejercicio 3 utilizando PDO - PHP Data Objects */

        //conexion a la BD
        try {
          $conexion = new PDO('pgsql:host=localhost;dbname=ejercicio1','postgres','postgres');
          //crear sentencia de consulta a la BD
          $query = "select p.nombre, p.precio, m.nombre, e.nombre, c.nombre
            from producto p
            join marca m on p.id_marca = m.id_marca
            join categoria c on p.id_categoria = c.id_categoria
            join empresa e on m.id_empresa = e.id_empresa
            order by 1";
          $resulquery = $conexion->prepare($query);
          //Ejecutar la sentencia de consulta
          $resulquery->execute();
          //Procesar resultado
          $impresion= $resulquery->fetchAll();
          echo "<table><tr><th>Nombre Producto</th><th>Precio</th><th>Marca</th><th>Empresa</th><th>Categoria</th></tr>";
          foreach ($impresion as $row) {
            echo "<tr>";
            echo "<td>$row[0]</td>";
            echo "<td>$row[1]</td>";
            echo "<td>$row[2]</td>";
            echo "<td>$row[3]</td>";
            echo "<td>$row[4]</td>";
            echo "</tr>";
          }
          echo "</table>";
          $conexion = null;
        } catch (PDOException $e) { //verificar que no hubo error
          echo "ERROR: ".$e->getMessage();
        }

    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
