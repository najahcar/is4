<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 4 - Ejercicio 3</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 3</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* Este archivo se encarga de conectar a la base de datos y traer un objeto PDO
    ================================
        Hacer un script en PHP que se conecte a la BD, anteriormente creada en el “ejercicio1” y
        haga lo siguiente:
        o Realizar una consulta a dicha BD para que traiga los siguientes datos (respetar
        orden):
        ▪ nombre_producto,precio_producto,nombre_marca,
        nombre_empresa,nombre_categoría

        o Presentar los datos de manera tabular (usando tablas HTML generadas a partir de
        PHP)

        Aclaración: Usar la extensión de pgsql */

        //conexion a la BD
        $conn_string = "host=localhost port=5432 dbname=ejercicio1 user=postgres password=postgres";
        $conexion = pg_connect($conn_string);
        //verificar que no hubo error de conexion
        if (!$conexion) {
          echo "Ocurrio un error de conexion";
          exit;
        }

        //consulta a la BD
        $sql = "select p.nombre, p.precio, m.nombre, e.nombre, c.nombre
          from producto p
          join marca m on p.id_marca = m.id_marca
          join categoria c on p.id_categoria = c.id_categoria
          join empresa e on m.id_empresa = e.id_empresa
          order by 1";
        $datos = pg_query($conexion, $sql);
        //verificar que no hubo error en la consulta
        if (!$datos) {
          echo "Ocurrio un error en la consulta";
          exit;
        }
        // imprimimos resultado en tabla
        echo "<table><tr><th>Nombre Producto</th><th>Precio</th><th>Marca</th><th>Empresa</th><th>Categoria</th></tr>";
        while($row = pg_fetch_array($datos)){
          echo "<tr>";
          echo "<td>$row[0]</td>";
          echo "<td>$row[1]</td>";
          echo "<td>$row[2]</td>";
          echo "<td>$row[3]</td>";
          echo "<td>$row[4]</td>";
          echo "</tr>";
        }
        echo "</table>";
        pg_close( $conexion);
    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
