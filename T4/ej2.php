<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 4 - Ejercicio 3</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
    <h1>Ejercicio 3</h1>
  </header>
  <nav>
    <a href="index.html">IR A INDEX</a>
  </nav>
  <div class="cuerpo">
    <?php
    /* • Instalar PostgreSQL en su última versión en su entorno de desarrollo.
• Instalar el pgAdmin (u otro cliente de base de datos que soporte el motor PostgreSQL).
• Conectarse con el servidor de PostgreSQL utilizando el cliente de conexión a base de datos
instalado en el paso previo.
• Crear una base de datos llamada “ejercicio1”
• Ejecutar el crebas de la base de datos (adjunto al ejercitario)
• Navegar en el pgAdmin y verificar todo lo creado con el script de creación.
• Cargar con el pgAdmin datos de prueba para la mencionada base de datos.
En la siguiente imagen se muestra el diagrama físico de la BD */

    ?>
    <img src="IMG/ej2.bmp" alt="Realizado los requisitos del ejercicio 2"  />
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
