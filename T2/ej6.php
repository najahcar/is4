<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tarea 2 - Ejercicio 6</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
      <h1>Ejercicio 6</h1>
    </header>
    <nav>
      <a href="index.html">IR A INDEX</a>
    </nav>
    <div class="cuerpo">
      <?php
      /* Hacer un script PHP el cual utilice el operador ternario de PHP para realizar lo siguiente:
      • Se deben declarar tres variables y le asignan valores enteros aleatorios (los valores deben
      estar entre 99 y 999). Las variables serán $a, $b y $c
      • Si la expresión $a*3 > $b+$c se debe imprimir que la expresión $a*3 es mayor que la
      expresión $b+$c
      • Si la expresión $a*3 <= $b+$c se debe imprimir que la expresión $b+$c es mayor o igual
      que la expresión $a*3 */
      mt_srand(time());
      $a=mt_rand(99,999);
      $b=mt_rand(99,999);
      $c=mt_rand(99,999);
      echo "Las variables aleatorias son: A=".$a.", B=".$b." y C=".$c.".";
      $mensaje= ($a*3 > $b+$c) ? 'La expresión $a*3 es mayor que la expresión $b+$c' : 'La expresión $b+$c es mayor o igualque la expresión $a*3';
      echo $mensaje;
      ?>
    </div>
    <div class="footer">
      <h3>Alumna: Najah Cardozo - C06135</h3>
    </div>
  </body>
</html>
