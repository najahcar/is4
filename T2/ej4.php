<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Tarea 2 - Ejercicio 4</title>
    <meta charset="UTF-8">
    <meta name="title" content="Tarea 2 - Ejercicio 4">
    <meta name="description" content="Tarea 2 - Ejercicio 4">
    <link href="CSS/style.css" rel="stylesheet" type="text/css"/>
  </head>
  <body>
    <header>
      <h1>Ejercicio 4</h1>
    </header>
    <nav>
      <a href="index.html">IR A INDEX</a>
    </nav>
    <div class="cuerpo">
      <?php
      $_var1=24.5;
      echo gettype($_var1), "<br />";
      $_var1='HOLA';
      echo gettype($_var1), "<br />";
      settype($_var1, "int");
      var_dump($_var1);
      ?>
    </div>

    <div class="footer">
      <h3>Alumna: Najah Cardozo - C06135</h3>
    </div>
  </body>
</html>
