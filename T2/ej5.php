<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Tarea 2 - Ejercicio 5</title>
    <meta charset="UTF-8">
    <meta name="title" content="Tarea 2 - Ejercicio 5">
    <meta name="description" content="Tarea 2 - Ejercicio 5">
    <link href="CSS/style.css" rel="stylesheet" type="text/css"/>
  </head>
  <body>
    <header>
      <h1>Ejercicio 5</h1>
    </header>
    <nav>
      <a href="index.html">IR A INDEX</a>
    </nav>
    <div class="cuerpo">
      <?php
      $_nro=9;
      echo "Tabla de multiplicar del ".$_nro.".";
      $_i=0;
      echo "<table>";
      while ($_i <= 10) {
        echo "<tr><td>$_nro x $_i = ".$_nro*$_i."</td></tr>";
        $_i++;
      }
      ?>
    </div>

    <div class="footer">
      <h3>Alumna: Najah Cardozo - C06135</h3>
    </div>
  </body>
</html>
