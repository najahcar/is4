<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tarea 2 - Ejercicio 8</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
      <h1>Ejercicio 8</h1>
    </header>
    <nav>
      <a href="index.html">IR A INDEX</a>
    </nav>
    <div class="cuerpo">
    <?php
    /* Hacer un script en PHP que imprima 900 números aleatorios pares
    Se deben generar números aleatorios entre 1 y 10.000 */
    mt_srand(time());
    $numero= mt_rand(1,10000);
    $esPar = ($numero%2==0) ? true : false ;
    $i=0;
    while ($i<900 && $numero==true ) {
      $numero= mt_rand(1,10000);
      $esPar = ($numero%2==0) ? true : false ;
      if ($esPar== true) {
        $i++;
        echo "$i- $numero. <br />";
      }else {
        continue;
      }
    }

    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
