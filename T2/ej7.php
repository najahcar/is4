<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tarea 2 - Ejercicio 7</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
      <h1>Ejercicio 7</h1>
    </header>
    <nav>
      <a href="index.html">IR A INDEX</a>
    </nav>
    <div class="cuerpo">
    <?php
      /* Hacer un script PHP, utilizando la estructura de selección swicth que realice lo siguiente:
      • Se deben definir tres variables correspondientes a las notas de un alumno en un curso de
      PHP
      • La variable parcial1 puede tener un valor entre 0 y 30 (Se debe generar un valor aleatorio)
      • La variable parcial2 puede tener un valor entre 0 y 20 (Se debe generar un valor aleatorio)
      • La variable final1 puede tener un valor entre 0 y 50 (Se debe generar un valor aleatorio)
      Se deben sumar los tres acumulados (en la expresión del switch) e imprimir si el alumno tuvo nota
      1 o 2 o 3 o 4 o 5
      Nota 1: entre 0 y 59
      Nota 2: entre 60 y 69
      Nota 3: entre 70 y 79
      Nota 4: entre 80 y 89
      Nota 5: entre 90 y 100 */
      mt_srand( time());
      $parcial1= mt_rand(0,30);
      $parcial2= mt_rand(0,20);
      $final1= mt_rand(0,50);
      switch ($acumulado= $parcial1+$parcial2+$final1) {
        case $acumulado>0 && $acumulado<60:
          echo "Los puntajes son:<br /> Parcial 1= $parcial1 <br /> Parcial 2= $parcial2 <br /> Final 1= $final1 <br /> Acumulado= $acumulado <br /> NOTA 1";
          break;
        case $acumulado>59 && $acumulado<70:
          echo "Los puntajes son:<br /> Parcial 1= $parcial1 <br /> Parcial 2= $parcial2 <br /> Final 1= $final1 <br /> Acumulado= $acumulado <br /> NOTA 2";
          break;
        case $acumulado>69 && $acumulado<80:
          echo "Los puntajes son:<br /> Parcial 1= $parcial1 <br /> Parcial 2= $parcial2 <br /> Final 1= $final1 <br /> Acumulado= $acumulado <br /> NOTA 3";
          break;
        case $acumulado>79 && $acumulado<90:
          echo "Los puntajes son:<br /> Parcial 1= $parcial1 <br /> Parcial 2= $parcial2 <br /> Final 1= $final1 <br /> Acumulado= $acumulado <br /> NOTA 4";
          break;
        case $acumulado>89 && $acumulado<100:
          echo "Los puntajes son:<br /> Parcial 1= $parcial1 <br /> Parcial 2= $parcial2 <br /> Final 1= $final1 <br /> Acumulado= $acumulado <br /> NOTA 5";
          break;
        default:
          echo "Verificar....";
          break;
      }
    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
