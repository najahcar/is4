<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Tarea 2 - Ejercicio 2</title>
    <meta charset="UTF-8">
    <meta name="title" content="Tarea 2 - Ejercicio 2">
    <meta name="description" content="Tarea 2 - Ejercicio 2">
    <link href="CSS/style.css" rel="stylesheet" type="text/css"/>
  </head>
  <body>
    <header>
      <h1>Ejercicio 2</h1>
    </header>
    <nav>
      <a href="index.html">IR A INDEX</a>
    </nav>
    <div class="cuerpo">
      <?php
      $_nombreyapellido='Najah Cardozo';
      $_nacionalidad='Paraguaya';
      echo '<p class="negriroja">Nombre y apellido: '.$_nombreyapellido.'</p>';
      print '<p class="subrayado">Nacionalidad: '.$_nacionalidad.'</p>';
      ?>
    </div>
    <div class="footer">
      <h3>Alumna: Najah Cardozo - C06135</h3>
    </div>
  </body>
</html>
