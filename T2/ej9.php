<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tarea 2 - Ejercicio 9</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    <header>
      <h1>Ejercicio 9</h1>
    </header>
    <nav>
      <a href="index.html">IR A INDEX</a>
    </nav>
    <div class="cuerpo">
    <?php
    /* Hacer un script en PHP que genere números aleatorios hasta que el número generado sea divisible
    por 983. Cuando ocurra esta condición imprimir un mensaje.
    Se debe usar un ciclo infinito */
    mt_srand(time());
    $numero= mt_rand();
    $divisible = ($numero%983==0) ? true : false ;
    while ($numero==true ) {
      $numero= mt_rand();
      $divisible = ($numero%983==0) ? true : false ;
      if ($divisible== true) {
        echo "$numero es divisible por 983. <br />";
        break;
      }else {
        continue;
      }
    }
    ?>
  </div>
  <div class="footer">
    <h3>Alumna: Najah Cardozo - C06135</h3>
  </div>
  </body>
</html>
